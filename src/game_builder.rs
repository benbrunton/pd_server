use pusoy_dos2::game::{
    Game,
    Ruleset,
    FlushPrecedence
};
use pusoy_dos2::cards::Suit;
use crate::data::{GameDetail, Ruleset as RulesetDef};

pub fn build(game_detail: GameDetail) -> Game {
    let ids = game_detail.users.iter().map(|player| {
        player.sub.clone()
    }).collect::<Vec<String>>();

    let (suit_order, ruleset) = match game_detail.ruleset {
        RulesetDef::Pickering => get_pickering_rules(),
        _                     => get_classic_rules()
    };

    Game::new(
        game_detail.decks as u8,
        game_detail.jokers as u8,
        &ids,
        suit_order,
        ruleset
    )
}

fn get_pickering_rules() -> ([Suit; 4], Ruleset) {
    ([
        Suit::Clubs,
        Suit::Hearts,
        Suit::Diamonds,
        Suit::Spades
    ],
    Ruleset {
        reversals_enabled: true,
        flush_precedence: FlushPrecedence::Rank
    })
}

fn get_classic_rules() -> ([Suit; 4], Ruleset) {
    ([
        Suit::Clubs,
        Suit::Spades,
        Suit::Hearts,
        Suit::Diamonds,
    ],
    Ruleset {
        reversals_enabled: false,
        flush_precedence: FlushPrecedence::Suit
    })

}

