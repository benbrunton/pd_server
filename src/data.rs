use r2d2_postgres::PostgresConnectionManager;
use r2d2::Pool;
use postgres::NoTls;
mod game;
mod user;

pub use self::game::*;
use self::user::*;

 
#[derive(Debug, Clone)]
pub struct Data {
    pub game: Game,
    pub user: User
}

impl Data {
    pub fn new(pool: Pool<PostgresConnectionManager<NoTls>>) -> Data {
        Data {
            game: Game::new(pool.clone()),
            user: User::new(pool.clone()),
        }
    }
}

