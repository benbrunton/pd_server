use actix_session::Session;
use actix_web::http::{StatusCode};
use log::{info, warn};

pub fn check_auth(session: Session) -> Result<String, StatusCode> {
    let user_session_result = session.get::<String>("sub");

    if user_session_result.is_err() {
        warn!("{:?}", user_session_result.err());
        return Err(StatusCode::UNAUTHORIZED);
    }

    let user_session = user_session_result.unwrap();
    if user_session.is_none() {
        info!("no sub in session");
        return Err(StatusCode::UNAUTHORIZED);
    }

    Ok(user_session.expect("unable to unwrap session"))
}
