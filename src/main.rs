use std::env;
use log::{info};
use env_logger;
use actix_web::{
    dev, http, guard, web, middleware, App, HttpServer,
    HttpResponse,
    middleware::cors::Cors,
    Result
};
use actix_session::CookieSession;
use actix_web::middleware::errhandlers::{
    ErrorHandlers, ErrorHandlerResponse
};
use actix_web::http::{StatusCode};
use serde::{Deserialize, Serialize};
use postgres::{NoTls, config::Config};
use r2d2_postgres::PostgresConnectionManager;


mod game_builder;
mod data;
mod controllers;
mod auth;

#[derive(Debug, Serialize, Deserialize)]
struct GenericResponse {
    message: String,
}

fn p404() -> Result<HttpResponse> {
    let body = GenericResponse{
        message: "not found".to_string()
    };

    let json_body = serde_json::to_string(&body).unwrap();
    let response = HttpResponse::build(StatusCode::NOT_FOUND)
        .content_type("application/json; charset=utf-8")
        .body(json_body);

    Ok(response)
}

fn main() -> std::io::Result<()> {
    env::set_var("RUST_LOG", "actix_web=debug,pd_server=debug");
    env_logger::init();

    info!("starting up");

    let allowed_origin = env::var("UI_HOST")
        .unwrap_or("https://api.soydos.test".to_string());

    let user = env::var("PG_USER").unwrap_or("pd".to_string());
    let pw = env::var("PG_PASSWORD")
        .unwrap_or("pusoydos".to_string());
    let database = env::var("PG_DATABASE")
        .unwrap_or("pusoydos".to_string());
    let host = env::var("PG_HOST")
        .unwrap_or("localhost".to_string());
    let config = Config::new()
        .user(&user)
        .password(&pw)
        .dbname(&database)
        .host(&host).to_owned();

    let manager = PostgresConnectionManager::new(
        config,
        NoTls,
    );

    let pool = r2d2::Pool::new(manager)
        .expect("unable to connect to pool");

    let data = data::Data::new(pool.clone());

    info!("data setup complete");

    HttpServer::new(move || App::new()
        .wrap(middleware::Logger::default())
        .wrap(
            Cors::new()
                .allowed_origin(&allowed_origin)
                .supports_credentials()
        )
        .wrap(
            CookieSession::signed(&[0; 32])
                    .secure(true)
        )
        .wrap(
            ErrorHandlers::new()
                .handler(
                    StatusCode::INTERNAL_SERVER_ERROR,
                    render_500
                )
        ).wrap(
            ErrorHandlers::new()
                .handler(
                    StatusCode::BAD_REQUEST,
                    render_400
                )
        )
        .data(data.clone())
        .service(
            web::resource("/game").route(
                web::post().to(controllers::game::create_game)
            )
        )
        .service(
            web::resource("/game/{game_id}")
                .route(
                    web::get().to(controllers::game::get_game)
                )
                .route(
                    web::post().to(controllers::game::submit_move)
                )
        )
        .service(
            web::resource("/game/join/{game_id}").route(
                web::post().to(controllers::game::join_game)
            )
        )
        .service(
            web::resource("/game/deal/{game_id}").route(
                web::post().to(controllers::game::deal_game)
            )
        )
        .service(
            web::resource("/redeem-token").route(
                web::post().to(controllers::login::redeem_token)
            )
        )
        .service(
            web::resource("/auth").route(
                web::get().to(controllers::login::auth)
            )
        )
        .service(
            web::resource("/current-games").route(
                web::get().to(controllers::user::current_games)
            )
        )
        .service(
            web::resource("/logout").route(
                web::post().to(controllers::login::logout)
            )
        )
        /*
        .service(
            web::scope("/protected").wrap(
                // /game
                // /current-games
                // /logout
                // /join-game
                // /begin-game
                // /submit-move
            )
        )
        */
        .default_service(
            // 404 for GET request
            web::resource("")
                .route(web::get().to(p404))
                // all requests that are not `GET`
                .route(
                    web::route()
                        .guard(guard::Not(guard::Get()))
                        .to(|| HttpResponse::MethodNotAllowed()),
                ),
        ))
        .bind("0.0.0.0:8080")?
        .run()
}

fn render_400<B>(mut res: dev::ServiceResponse<B>) -> Result<ErrorHandlerResponse<B>> {
    res.response_mut()
       .headers_mut()
       .insert(http::header::CONTENT_TYPE, http::HeaderValue::from_static("Error"));
    Ok(ErrorHandlerResponse::Response(res))
}

fn render_500<B>(mut res: dev::ServiceResponse<B>) -> Result<ErrorHandlerResponse<B>> {
    res.response_mut()
       .headers_mut()
       .insert(http::header::CONTENT_TYPE, http::HeaderValue::from_static("Error"));
    Ok(ErrorHandlerResponse::Response(res))
}

