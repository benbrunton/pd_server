use serde::{Deserialize, Serialize};
use actix_web::{ web, Responder };
use actix_session::Session;
use actix_web::http::{StatusCode};
use crate::data::{Data, GameInfo};
use crate::auth::check_auth;
use log::info;

#[derive(Debug, Serialize, Deserialize)]
pub struct GamesResponse {
    games: Option<Vec<GameInfo>>,
}

pub fn current_games(
    data: web::Data<Data>,
    session: Session
) -> impl Responder {
    let err_response = GamesResponse{
        games: None,
    };

    let auth_result = check_auth(session);
    if auth_result.is_err() {
        let status_code = auth_result.err()
            .expect("it was supposed to err");
        return web::Json(err_response).with_status(status_code);
    }

    info!("get current games");

    let sub = auth_result.unwrap();
    let games_result = data.game.get_current_games(sub);

    let games = games_result.expect("unable to get games");

    web::Json(GamesResponse{ games: Some(games) })
        .with_status(StatusCode::OK)
}

