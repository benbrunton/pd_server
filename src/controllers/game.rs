use serde::{Deserialize, Serialize};
use crate::data::{Data, Status, Ruleset};
use log::{info, warn};
use actix_web::{ web, Responder };
use actix_web::http::{StatusCode};
use actix_session::Session;
use crate::auth::check_auth;
use crate::game_builder::build;
use pusoy_dos2::cards::{PlayedCard, Card, Suit, Rank};
use pusoy_dos2::game::{Hand};

#[derive(Debug, Serialize, Deserialize)]
pub struct GameRequest {
    decks: i32,
    jokers: i32,
    ruleset: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewGameResponse {
    message: String,
    id: Option<i32>
}

pub fn create_game(
    game: web::Json<GameRequest>,
    data: web::Data<Data>,
    session: Session
) -> impl Responder {

    let err_response = NewGameResponse{
        message: "unauthorised".to_string(),
        id: None,
    };

    let auth_result = check_auth(session);
    if auth_result.is_err() {
        let status_code = auth_result.err()
            .expect("it was supposed to err");
        return web::Json(err_response).with_status(status_code);
    }


    info!("new game: {:?}", game);

    let sub = auth_result.unwrap();

    let id = data.game.create_game(
        &sub, game.decks, game.jokers, &game.ruleset
    ).expect("unable to create game");


    data.game.add_user_to_game(
        id, &sub
    ).expect("unable to add user");

    let body = NewGameResponse{
        message: "new game created".to_string(),
        id: Some(id)
    };
    web::Json(body).with_status(StatusCode::OK)
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Info{
    game_id: i32
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GameResponse{
    id: u32,
    created_user: bool,
    in_game: bool,
    ruleset: Ruleset,
    status: Status,
    users: Vec<GameUser>,
    decks: i32,
    jokers: i32,
    hand: Vec<Card>,
    last_move: Option<Hand>,
    next_player: Option<String>,
    winners: Vec<String>,
    suit_order: Option<[Suit; 4]>,
    rank_order: Option<[Rank; 13]>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GameUser {
    pub sub: String,
    pub name: String,
    pub picture: String,
    pub card_count: i32,
}

pub fn get_game(
    info: web::Path<Info>,
    data: web::Data<Data>,
    session: Session
) -> impl Responder {
    let id = info.game_id;
    let err_response = GameResponse{
        created_user: false,
        in_game: false,
        id: id as u32,
        ruleset: Ruleset::Pickering,
        status: Status::Pending,
        users: vec!(),
        decks: 0,
        jokers: 0,
        hand: vec!(),
        last_move: None,
        next_player: None,
        winners: vec!(),
        suit_order: None,
        rank_order: None,
    };

    let auth_result = check_auth(session);
    if auth_result.is_err() {
        let status_code = auth_result.err()
            .expect("it was supposed to err");
        return web::Json(err_response).with_status(status_code);
    }

    let user_id = auth_result.expect("unable to get user id");

    let game_result = data.game.get_game(id);
    let game = game_result.expect("unable to unwrap game");
    let created_user = user_id == game.created_user;
    let in_game = game.users.iter().map(|user| 
        { user.sub.to_string() }).collect::<String>()
        .contains(&user_id);

    let mut users = game.users.iter().map(|user| {
        GameUser{
            sub: user.sub.clone(),
            name: user.name.clone(),
            picture: user.picture.clone(),
            card_count: 0
        }
    }).collect::<Vec<GameUser>>();

    let mut hand = vec!();
    let mut last_move = None;
    let mut next_player = None;
    let mut winners = vec!();
    let mut suit_order = None;
    let mut rank_order = None;
    if game.game.is_some() {
        let mut users_with_count = vec!();
        let game_def = game.game.expect("can't unwrap gamedef");
        for user in users {
            let card_count = game_def.get_player(&user.sub)
                .expect("unable to get player")
                .get_card_count() as i32;
            let users_info = GameUser{
                sub: user.sub.clone(),
                name: user.name.clone(),
                picture: user.picture.clone(),
                card_count
            };
            users_with_count.push(users_info);
        }
        users = users_with_count;

        hand = game_def.get_player(&user_id).unwrap().get_hand();
        last_move = game_def.get_last_move();
        next_player = game_def.get_next_player();
        winners = game_def.get_winners();
        suit_order = Some(game_def.get_suit_order());
        rank_order = Some(game_def.get_rank_order());
    }

    let response = GameResponse{
        created_user,
        in_game,
        id: id as u32,
        ruleset: game.ruleset,
        status: game.status,
        users,
        decks: game.decks,
        jokers: game.jokers,
        hand,
        last_move,
        next_player,
        winners,
        suit_order,
        rank_order,
    };

    web::Json(response).with_status(StatusCode::OK)
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ActionResponse{
    success: bool
}

pub fn join_game(
    info: web::Path<Info>,
    data: web::Data<Data>,
    session: Session
) -> impl Responder {
    let id = info.game_id;
    let err_response = ActionResponse{
        success: false,
    };

    let auth_result = check_auth(session);
    if auth_result.is_err() {
        let status_code = auth_result.err()
            .expect("it was supposed to err");
        return web::Json(err_response).with_status(status_code);
    }

    let user_id = auth_result.expect("unable to get user id");

    // check if game exists and if user is already joined
    let game_result = data.game.get_game(id);

    if game_result.is_err() {
        warn!("unable to get game_result for {}", id);
        return web::Json(err_response)
            .with_status(StatusCode::INTERNAL_SERVER_ERROR);
    }

    let game = game_result.expect("unable to unwrap game");
    for user in game.users.iter() {
        if user.sub == user_id {
            return web::Json(err_response)
                .with_status(StatusCode::BAD_REQUEST);
        }
    }
    

    let _ = data.game.add_user_to_game(
        id,
        &user_id
    );

    web::Json(ActionResponse{
        success: true
    }).with_status(StatusCode::OK)
}

pub fn deal_game(
    info: web::Path<Info>,
    data: web::Data<Data>,
    session: Session
) -> impl Responder {
    let id = info.game_id;
    let err_response = ActionResponse{
        success: false,
    };

    let auth_result = check_auth(session);
    if auth_result.is_err() {
        let status_code = auth_result.err()
            .expect("it was supposed to err");
        return web::Json(err_response).with_status(status_code);
    }

    let user_id = auth_result.expect("unable to get user id");
    let game_result = data.game.get_game(id);

    if game_result.is_err() {
        return web::Json(err_response)
            .with_status(StatusCode::INTERNAL_SERVER_ERROR);
    }

    let game_details = game_result.expect("unable to unwrap game");

    if game_details.created_user != user_id {
         return web::Json(err_response)
            .with_status(StatusCode::UNAUTHORIZED);
    }


    info!("building game {}", id);
    let g = build(game_details);

    let _ = data.game.update_game(
        id,
        g,
        Status::Active
    );


    web::Json(ActionResponse{
        success: true
    }).with_status(StatusCode::OK)

}

#[derive(Debug, Serialize, Deserialize)]
pub struct SubmitRequest {
    hand: Vec<PlayedCard>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SubmitResponse {
    success: bool,
}

pub fn submit_move(
    info: web::Path<Info>,
    req: web::Json<SubmitRequest>,
    data: web::Data<Data>,
    session: Session
) -> impl Responder {
    let id = info.game_id;
    let err_response = SubmitResponse{
        success: false,
    };
    let auth_result = check_auth(session);
    if auth_result.is_err() {
        let status_code = auth_result.err()
            .expect("it was supposed to err");
        return web::Json(err_response).with_status(status_code);
    }

    let user_id = auth_result.expect("unable to get user id");

    let game_result = data.game.get_game(id);
    let game = game_result.expect("unable to unwrap game");
    let mut game_def = game.game.expect("unable to get game");

    let _ = game_def.play_move(&user_id, req.hand.clone());

    // if next_player == None then 
    let next_player = game_def.get_next_player();

    let status = if next_player == None {
        Status::Complete
    } else {
        Status::Active
    };

    info!("updating game {}", id);
    let _ = data.game.update_game(
        id,
        game_def,
        status
    );

    info!("game {} updated", id);

    web::Json(SubmitResponse{
        success: true
    }).with_status(StatusCode::OK)
   
}

