use serde::{Deserialize, Serialize};
use actix_web::{ web, Responder };
use actix_web::http::{StatusCode};
use actix_session::Session;
use reqwest;
use reqwest::header;
use log::{info, warn};

use crate::data::Data;
use crate::auth::check_auth;

#[derive(Debug, Serialize, Deserialize)]
pub struct TokenRequest {
    token: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RedeemTokenResponse {
    message: String,
    user: Option<UserInfo>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserInfo {
    sub: String,
    name: String,
    picture: String,
    email: String
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AuthResponse {
    user: Option<UserInfo>,
    logged_in: bool
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GenericResponse {
    success: bool
}

pub fn auth(
    _data: web::Data<Data>,
    session: Session
) -> impl Responder {

    let err_response = AuthResponse{
        user: None,
        logged_in: false
    };

    let auth_result = check_auth(session);
    if auth_result.is_err() {
        let status_code = auth_result.err()
            .expect("it was supposed to err");
        return web::Json(err_response).with_status(status_code);
    }

    let sub = auth_result.unwrap();


    web::Json(AuthResponse{
        user: Some(UserInfo{
            sub,
            name: "My Name".to_string(),
            picture: "".to_string(),
            email: "".to_string(),
        }), 
        logged_in: true,
    }).with_status(StatusCode::OK)
}


pub fn redeem_token(
    req: web::Json<TokenRequest>,
    data: web::Data<Data>,
    session: Session,
) -> impl Responder {

    let user_result = get_userinfo(&req.token);

    if user_result.is_err() {
        return web::Json(RedeemTokenResponse{
            message: "unable to login".to_string(),
            user: None,
        }).with_status(StatusCode::UNAUTHORIZED);
    }

    let user = user_result.expect("unable to unwrap user");
    let response_user = Some(user.clone());
    let session_user = user.clone();

    info!("upserting user");
    let upsert_result = data.user.upsert_user(
        user.sub,
        user.name,
        user.picture,
        user.email
    );

    if upsert_result.is_err() {
        warn!("{:?}", upsert_result.err().unwrap());
        return web::Json(RedeemTokenResponse{
            message: "unable to login".to_string(),
            user: None,
        }).with_status(StatusCode::UNAUTHORIZED);
    }

    session.set("sub", session_user.sub)
        .expect("sub not set in session");

    info!("responding with user info");

    let body = RedeemTokenResponse{
        message: "logged in user".to_string(),
        user: response_user,
    };

    web::Json(body).with_status(StatusCode::OK)
}

pub fn logout(session: Session) -> impl Responder {
    session.clear();
    web::Json(GenericResponse{
        success: true
    }).with_status(StatusCode::OK)
}


fn get_userinfo(token: &str)  -> Result<UserInfo, reqwest::Error> {
    let bearer_token = format!("Bearer {}", token);
    let mut headers = header::HeaderMap::new();
    headers.insert(
        header::AUTHORIZATION,
        header::HeaderValue::from_str(&bearer_token).unwrap());

    let client = reqwest::Client::builder()
        .default_headers(headers)
        .build().expect("unable to build request client");
    let mut res = client.get(
        "https://soydos.eu.auth0.com/userinfo"
    ).send().expect("unable to send request");
    info!("response received from auth0 userinfo");
    res.json::<UserInfo>()
}
