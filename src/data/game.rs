use std::time::SystemTime;
use chrono::{Utc, TimeZone};
use postgres::{NoTls, Error};
use r2d2_postgres::PostgresConnectionManager;
use postgres_derive::{ToSql, FromSql};
use r2d2::Pool;
use serde::{Deserialize, Serialize};
use serde_json::{json, value::Value};
use log::{info, warn};
use pusoy_dos2::game::Game as PDGame;

macro_rules! to_sql_checked {
    () => {
        fn to_sql_checked(&self,
                          ty: &postgres::types::Type,
                          out: &mut ::std::vec::Vec<u8>)
                          -> ::std::result::Result<postgres::types::IsNull,
                                                   Box<dyn ::std::error::Error +
                                                       ::std::marker::Sync +
                                                       ::std::marker::Send>> {
            postgres::types::__to_sql_checked(self, ty, out)
        }
    }
}

#[derive(Debug, FromSql, ToSql, Serialize, Deserialize)]
#[postgres(name = "ruleset")]
pub enum Ruleset {
    #[postgres(name = "pickering")]
    Pickering,
    #[postgres(name = "classic")]
    Classic,
}

impl Ruleset {
    pub fn from(rule: &str) -> Ruleset {
        match rule {
            "pickering" => Ruleset::Pickering,
            _           => Ruleset::Classic,
        }
    }
}

#[derive(Debug, FromSql, ToSql, Serialize, Deserialize)]
#[postgres(name = "status")]
pub enum Status {
    #[postgres(name = "pending")]
    Pending,
    #[postgres(name = "active")]
    Active,
    #[postgres(name = "complete")]
    Complete
}

/// Summary info
#[derive(Debug, Serialize, Deserialize)]
pub struct GameInfo {
    id: i32,
    ruleset: Ruleset,
    status: Status,
    created: String,
    created_name: String,
    next_player_id: String,
    next_player_name: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserDetails {
    pub sub: String,
    pub name: String,
    pub picture: String,
}

/// individual game detail
#[derive(Debug, Serialize, Deserialize)]
pub struct GameDetail {
    pub id: i32,
    pub ruleset: Ruleset,
    pub status: Status,
    pub users: Vec<UserDetails>,
    pub decks: i32,
    pub jokers: i32,
    pub created_user: String,
    pub game: Option<PDGame>
}

#[derive(Debug, Clone)]
pub struct Game {
    pool: Pool<PostgresConnectionManager<NoTls>>
}

impl Game {
    pub fn new(pool: Pool<PostgresConnectionManager<NoTls>>) -> Game {
        Game { pool }
    }

    pub fn create_game(
        &self,
        created_user: &str,
        decks: i32,
        jokers: i32,
        ruleset_str: &str) -> Result<i32, ()> {
        let mut client = self.pool.get().unwrap();
        let ruleset = Ruleset::from(&ruleset_str);
        let result = client.query(
            "INSERT INTO pusoydos.game 
                (created_user, decks, jokers, ruleset, status)
                VALUES ($1, $2, $3, $4, 'pending')
                RETURNING id",
            &[&created_user, &decks, &jokers, &ruleset],
        ).unwrap();

        let id: i32 = result.into_iter()
            .nth(0).expect("no rows returned").get(0);
        Ok(id)
    } 

    pub fn add_user_to_game(
        &self,
        game_id: i32,
        user_id: &str) -> Result<(), ()> {
        let mut client = self.pool.get().unwrap();
        let _ = client.query(
            "INSERT INTO pusoydos.game_user
                (game_id, user_id, user_name)
                VALUES ($1, $2, 'User Name')",
            &[&game_id, &user_id],
        );

        Ok(())
    }

    pub fn get_current_games(
        &self,
        sub: String,
    ) -> Result<Vec<GameInfo>, ()> {
        let mut client = self.pool.get().unwrap();
        let result = client.query(
            "SELECT game_id, g.ruleset, g.status, g.created, u.name as created_name, g.next_player as next_player_id, u2.name as next_player_name
                FROM pusoydos.game_user
                LEFT JOIN pusoydos.game AS g ON game_id = g.id
                LEFT JOIN pusoydos.\"user\" AS u ON g.created_user = u.sub
                LEFT JOIN pusoydos.\"user\" AS u2 ON g.next_player = u2.sub
                WHERE user_id = $1
                AND g.status IN ('pending', 'active')",
            &[&sub],
        );


        if result.is_err() {
            warn!("{:?}", result.err());
            return Err(())
        }

        let games = result.expect("unable to retrieve current games");

        info!("selecting current games..");
        Ok(games.iter().map(|row| {
            let created_time: SystemTime = row.get("created");
            let created_secs = created_time
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs() as i64;
            let created = Utc.timestamp(
                created_secs, 0
            ).to_string();

            let next_player_id_result: Result<String, Error> = row
                .try_get("next_player_id");
            let next_player_id = if next_player_id_result.is_ok() {
                next_player_id_result.expect(
                    "unable to unwrap next_player_id"
                )
            } else {
                "".to_string()                
            };

            let next_player_name_result: Result<String, Error> = row
                .try_get("next_player_name");
            let next_player_name = if next_player_name_result.is_ok() {
                next_player_name_result.expect(
                    "unable to unwrap next_player_name"
                )
            } else {
                "".to_string()                
            };

            GameInfo{
                id: row.get("game_id"),
                ruleset: row.get("ruleset"),
                status: row.get("status"),
                created_name: row.get("created_name"),
                created,
                next_player_id,
                next_player_name,
            }
        }).collect())
    }

    pub fn get_game(
        &self,
        id: i32
    ) -> Result<GameDetail, ()> {
        let mut client = self.pool.get().unwrap();
        let result = client.query(
            "select 
                id,
                ruleset,
                status,
                created_user,
                decks,
                jokers,
                c.user_id,
                u.name,
                u.picture,
                current_state
             from pusoydos.game
             left join pusoydos.game_user as c on c.game_id = id
             left join pusoydos.user as u on u.sub = c.user_id
             where game_id = $1",
            &[&id],
        );

        if result.is_err() {
            warn!("{:?}", result.err());
            return Err(())
        }

        let game = result
            .expect("unable to retrieve current games");

        if game.len() < 1 {
            warn!("no games with id {}", id);
            return Err(())
        }

        info!("select game {}", id);
        let users = game.iter().map(|row| {
            UserDetails{
                sub: row.get("user_id"),
                name: row.get("name"),
                picture: row.get("picture"),
            }
        }).collect();

        let first_row = game.first().expect("no first row!");
        let game_obj_result: Result<Value, Error> = first_row
            .try_get("current_state");
        let game_obj = if game_obj_result.is_ok() {
            info!("game found {}", id);
            let g = game_obj_result.expect(
                "unable to unwrap current state"
            );
            Some(serde_json::from_value(g)
                .expect("unable to deserialize game"))
        } else {
            info!("no game found {}", id);
            info!("{:?}", game_obj_result.err());
            None 
        };

        Ok(GameDetail{
            id: id as i32,
            ruleset: first_row.get("ruleset"),
            status: first_row.get("status"),
            users,
            decks: first_row.get("decks"),
            jokers: first_row.get("jokers"),
            created_user: first_row.get("created_user"),
            game: game_obj
        })
    }

    pub fn update_game(
        &self,
        id: i32,
        game: PDGame,
        status: Status
    ) -> Result<(), ()> {
        let next_player = game.get_next_player();
        let json_game = json!(game);
        let mut client = self.pool.get().unwrap();
        let result = client.query(
            "UPDATE pusoydos.game 
                SET current_state = $1,
                    status = $2,
                    next_player = $3
                WHERE
                    id = $4",
            &[&json_game, &status, &next_player, &id]
        );

        if result.is_err() {
            warn!("{:?}", result.err());
            return Err(());
        }

        Ok(())
    }
}
