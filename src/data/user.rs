use postgres::{NoTls, error::Error as PgError};
use r2d2_postgres::PostgresConnectionManager;
use r2d2::Pool;

#[derive(Debug, Clone)]
pub struct User {
    pool: Pool<PostgresConnectionManager<NoTls>>
}

impl User {
    pub fn new(pool: Pool<PostgresConnectionManager<NoTls>>) -> User {
        User { pool }
    }

    pub fn upsert_user(
        &self,
        sub: String,
        name: String,
        picture: String,
        email: String,
    ) -> Result<(), PgError> {
        if self.does_user_exist(&sub) {
            self.update_user(&sub, &picture, &email)
        } else {
            self.insert_user(&sub, &name, &picture, &email)
        }
    }

    fn does_user_exist(
        &self,
        sub: &str,
    ) -> bool {
        let mut client = self.pool.get().unwrap();
        let result = client.query(
            "SELECT exists(SELECT 1 
              FROM pusoydos.user
              WHERE sub = $1)",
            &[&sub],
        ).unwrap();

        return result.into_iter()
            .nth(0).expect("no rows returned").get(0);
    }

    // don't update name
    fn update_user(
        &self,
        sub: &str,
        picture: &str,
        email: &str,
    ) -> Result<(), PgError> {
        let mut client = self.pool.get().unwrap();
        let result = client.query(
            "UPDATE pusoydos.user
                SET picture = $2,
                    email = $3
                WHERE
                    sub = $1",
            &[&sub, &picture, &email],
        );

        return if result.is_ok() {
            Ok(())
        } else {
            Err(result.err().unwrap())
        };
    }

    fn insert_user(
        &self,
        sub: &str,
        name: &str,
        picture: &str,
        email: &str,
    ) -> Result<(), PgError> {
        let mut client = self.pool.get().unwrap();
        let result = client.query(
            "INSERT INTO pusoydos.user
                (sub, name, picture, email)
                VALUES ($1, $2, $3, $4)",
            &[&sub, &name, &picture, &email],
        );

        return if result.is_ok() {
            Ok(())
        } else {
            Err(result.err().unwrap())
        };
    }
}
