# You can override this `--build-arg BASE_IMAGE=...` to use different
# version of Rust or OpenSSL.
ARG BASE_IMAGE=ekidd/rust-musl-builder:latest

FROM ${BASE_IMAGE} AS builder

ADD . ./
RUN sudo rm -rf .cargo
RUN sudo chown -R rust:rust /home/rust

RUN cargo build --release

# -----------------------------------------

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder \
    /home/rust/src/target/x86_64-unknown-linux-musl/release/pd-server \
    /usr/local/bin/
CMD /usr/local/bin/pd-server
